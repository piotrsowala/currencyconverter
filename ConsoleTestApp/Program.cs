﻿using CurrencyConverter.Sandbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTestApp
{
    public struct Manufacturer
    {
        public int Employees { get; set; }
        public string Name { get; set; }
    }


    public interface IVehicle
    {
        Manufacturer Manufacturer { get; set; }
        string Model { get; set; }
        int HorsePower { get; set; }
    }

    public class Car : IVehicle
    {
        public Manufacturer Manufacturer { get; set; }
        public string Model { get; set; }
        public int HorsePower { get; set; }
    }

    public struct Bike : IVehicle
    {
        public Manufacturer Manufacturer { get; set; }
        public string Model { get; set; }
        public int HorsePower { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ///// TEST 1

            Car car1 = new Car
            {
                Model = "Volkswagen Golf",
                HorsePower = 140
            };

            Bike bike1 = new Bike
            {
                Model = "Honda VFR",
                HorsePower = 100
            };

            UpdateVehicle(car1);
            UpdateVehicle(bike1);

            Console.WriteLine($"Car power  = {car1.HorsePower}");
            Console.WriteLine($"Car power  = {bike1.HorsePower}");


            ///// TEST 2*

            IVehicle car2 = new Car
            {
                Model = "Toyota Avensis",
                HorsePower = 120
            };

            IVehicle bike2 = new Bike
            {
                Model = "Ducati Monster",
                HorsePower = 80
            };

            UpdateVehicle(car2);
            UpdateVehicle(bike2);

            Console.WriteLine($"Bike power = {car2.HorsePower}");
            Console.WriteLine($"Bike power = {bike2.HorsePower}");


            Console.ReadLine();
        }

        static void UpdateCar(Car vehicle)
        {
            vehicle.HorsePower = (int)(vehicle.HorsePower * 1.25m);
            vehicle.Manufacturer.Name = "aaa";
            vehicle.Manufacturer.Employees = 4;

            vehicle.Manufacturer = new Manufacturer();

        }

        static void UpdateBike(Bike vehicle)
        {
            vehicle.HorsePower = (int)(vehicle.HorsePower * 1.25m);
            vehicle.Manufacturer.Name = "aaa";
            vehicle.Manufacturer.Employees = 4;

            vehicle.Manufacturer = new Manufacturer();
        }

        static void UpdateVehicle(IVehicle vehicle)
        {
            vehicle.HorsePower = (int)(vehicle.HorsePower * 1.25m);
            vehicle.Manufacturer.Name = "aaa";
            vehicle.Manufacturer.Employees = 4;

            vehicle.Manufacturer = new Manufacturer();
        }
    }
}
