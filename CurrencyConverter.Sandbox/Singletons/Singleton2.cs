﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Sandbox.Singletons
{
    /// <summary>
    /// Thread-safe, but not so fast
    /// </summary>
    public sealed class Singleton2
    {
        private Singleton2()
        {

        }

        private static Singleton2 _instance;
        private static readonly object _padlock = new object();

        public static Singleton2 GetInstance()
        {
            lock(_padlock) 
            {
                if(_instance == null)
                {
                    _instance = new Singleton2();
                }
                return _instance;
            }
        }
    }
}
