﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Sandbox.Singletons
{
    /// <summary>
    /// The simpliest, not thread-safe singleton
    /// </summary>
    public sealed class Singleton1
    {
        private Singleton1()
        {

        }

        private static Singleton1 _instance;

        public static Singleton1 GetInstance()
        {
            if(_instance == null)
            {
                _instance = new Singleton1();
            }
            return _instance;
        }
    }
}
