﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Sandbox.Tools
{
    public static class ExceptionHelper
    {
        private static IDictionary<Type, int> ExceptionCounter = new Dictionary<Type, int>();


        public static void ThrowException<T>(int maxCount) where T : Exception, new()
        {
            ExceptionCounter.TryGetValue(typeof(T), out int count);
            if (count < maxCount)
            {
                var exc = Activator.CreateInstance<T>();
                ExceptionCounter[typeof(T)] = ++count;
                throw exc;
            }
        }
    }
}
