﻿using System;
using System.Linq;
using CurrencyConverter.Server.Utils;
using NUnit.Framework;

namespace CurrencyConverter.Server.Tests
{
    [TestFixture]
    public class NumberEnumTests
    {
        [Test]
        public void NumberEnum_AllItemsHaveDisplayName()
        {
            // arrange
            var items = Enum.GetValues(typeof(NumberEnum)).Cast<NumberEnum>();

            foreach (var enumValue in items)
            {
                // act
                var display = enumValue.GetDisplay();

                // assert
                Assert.IsNotNull(display);
                Assert.IsNotEmpty(display);
            }
        }
    }
}
