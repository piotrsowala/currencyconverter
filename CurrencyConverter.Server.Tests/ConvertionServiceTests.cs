﻿using System.ServiceModel;
using CurrencyConverter.Server.DataContracts;
using CurrencyConverter.Server.Services;
using NUnit.Framework;

namespace CurrencyConverter.Server.Tests
{
    [TestFixture]
    public class ConvertionServiceTests
    {
        private ConvertionService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new ConvertionService();
        }

        [TestCase("0", ExpectedResult = "zero dollars")]
        [TestCase("1", ExpectedResult = "one dollar")]
        [TestCase("25,1", ExpectedResult = "twenty-five dollars and ten cents")]
        [TestCase("0,01", ExpectedResult = "zero dollars and one cent")]
        [TestCase("45100", ExpectedResult = "forty-five thousand one hundred dollars")]
        [TestCase("999999999,99", ExpectedResult = "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents")]
        public string ConvertionService_Convert_PositiveTest1(string input)
        {
            // act
            return _service.Convert(input);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("-5")]
        [TestCase("-30003,33")]
        [TestCase("6,450")]
        [TestCase("123,456")]
        [TestCase("1234567890")]
        [TestCase("12.34")]
        [TestCase("safsadfa")]
        public void ConvertionService_Convert_ExceptionTest1(string input)
        {
            // act & assert
            Assert.Throws<FaultException<ValidationFault>>(() => _service.Convert(input));
        }
    }
}
