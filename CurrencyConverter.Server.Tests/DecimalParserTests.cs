﻿using System;
using CurrencyConverter.Server.Utils;
using NUnit.Framework;

namespace CurrencyConverter.Server.Tests
{
    [TestFixture]
    public class DecimalParserTests
    {
        private DecimalParser _parserObject;

        [SetUp]
        public void SetUp()
        {
            _parserObject = new DecimalParser();
        }

        [Test]
        public void DecimalParser_Parse_Test1()
        {
            // arrange
            var input = "0";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(0m, result);
        }

        [Test]
        public void DecimalParser_Parse_Test2()
        {
            // arrange
            var input = "1";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(1m, result);
        }

        [Test]
        public void DecimalParser_Parse_Test3()
        {
            // arrange
            var input = "25,1";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(25.1m, result);
        }

        [Test]
        public void DecimalParser_Parse_Test4()
        {
            // arrange
            var input = "0,01";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(0.01m, result);
        }

        [Test]
        public void DecimalParser_Parse_Test5()
        {
            // arrange
            var input = "45100";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(45100m, result);
        }

        [Test]
        public void DecimalParser_Parse_Test6()
        {
            // arrange
            var input = "999999999,99";

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual(999999999.99m, result);
        }


        [TestCase("1,304.16")]
        [TestCase("$1,456.78")]
        [TestCase("1,094")]
        [TestCase("-152")]
        [TestCase("123,45 €")]
        [TestCase("1 304,16")]
        [TestCase("Ae9f")]
        [TestCase("542234234234231,4")]
        [TestCase("-5231,48")]
        public void DecimalParser_Parse_ExceptionTest1(string input)
        {
            // act && assert
            Assert.Catch<ArgumentException>(() => _parserObject.Parse(input));
        }
    }
}
