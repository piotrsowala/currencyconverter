﻿using System;
using CurrencyConverter.Server.Utils;
using NUnit.Framework;

namespace CurrencyConverter.Server.Tests
{
    [TestFixture]
    public class CurrencyParserTests
    {
        private CurrencyParser _parserObject;

        [SetUp]
        public void SetUp()
        {
            _parserObject = new CurrencyParser();
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest1()
        {
            // arrange
            decimal input = 0m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("zero dollars", result);
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest2()
        {
            // arrange
            decimal input = 1m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("one dollar", result);
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest3()
        {
            // arrange
            decimal input = 25.1m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("twenty-five dollars and ten cents", result);
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest4()
        {
            // arrange
            decimal input = 0.01m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("zero dollars and one cent", result);
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest5()
        {
            // arrange
            decimal input = 45100m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("forty-five thousand one hundred dollars", result);
        }

        [Test]
        public void CurrencyParser_Parse_PositiveTest6()
        {
            // arrange
            decimal input = 999999999.99m;

            // act
            var result = _parserObject.Parse(input);

            // assert
            Assert.AreEqual("nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents", result);
        }





        [Test]
        public void CurrencyParser_Parse_ExceptionTest1()
        {
            // arrange
            decimal input = -33.3m;

            // act && assert
            Assert.Catch<ArgumentException>(() => _parserObject.Parse(input));
        }
    }
}
