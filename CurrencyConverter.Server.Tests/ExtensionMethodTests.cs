﻿using System.ComponentModel.DataAnnotations;
using CurrencyConverter.Server.Utils;
using NUnit.Framework;

namespace CurrencyConverter.Server.Tests
{
    [TestFixture]
    public class ExtensionMethodTestsTests
    {
        [Test]
        public void ExtensionMethods_GetAttribute_Test1()
        {
            // arrange
            var enumValue = NumberEnum.N13;

            // act
            var attribute = enumValue.GetAttribute<DisplayAttribute>();

            // assert
            Assert.NotNull(attribute);
        }

        [Test]
        public void ExtensionMethods_GetDisplay_Test1()
        {
            // arrange
            var enumValue = NumberEnum.N0;

            // act
            var display = enumValue.GetDisplay();

            // assert
            Assert.AreEqual("zero", display);
        }
    }
}
