﻿using CurrencyConverter.Client.ConvertionService;
using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows;

namespace CurrencyConverter.Client.Services
{
    public class ConvertionServiceProxy
    {
        private readonly ConvertionServiceClient _client;

        public ConvertionServiceProxy()
        {
            _client = new ConvertionServiceClient();
        }

        public async Task<string> Convert(string number)
        {
            try
            {
                return await _client.ConvertAsync(number);
            }
            catch (FaultException<ValidationFault> ex)
            {
                MessageBox.Show(ex.Detail.Message);
            }
            catch (FaultException ex)
            {
                HandleException(ex);
            }
            catch (TimeoutException ex)
            {
                HandleException(ex);
            }
            catch(CommunicationException ex)
            {
                HandleException(ex);
            }

            return await Task.FromResult<string>(null);
        }

        private void HandleException(Exception ex)
        {
            var message = $"{nameof(ConvertionServiceProxy)} - {ex.GetType()}: {ex.Message}";

            // logger substitute
            MessageBox.Show(message);
        }
    }
}
