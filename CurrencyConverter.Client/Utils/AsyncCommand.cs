﻿using System;
using System.Threading.Tasks;

namespace CurrencyConverter.Client.Utils
{
    public class AsyncCommand : IAsyncCommand
    {
        public AsyncCommand(Func<Task> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        private readonly Func<Task> _execute;
        private readonly Func<object, bool> _canExecute;

        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        public Task ExecuteAsync(object parameter)
        {
            return _execute?.Invoke() ?? null;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter) ?? true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
