﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace CurrencyConverter.Client.Utils
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}
