﻿using System.Windows;
using System.Windows.Threading;

namespace CurrencyConverter.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {          
            var viewModel = new MainWindowViewModel();
            var view = new MainWindow();
            view.DataContext = viewModel;
            view.Show();
        }

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            var message = $"Globally handled - {e.Exception.GetType()}: {e.Exception.Message}";

            // logger substitute
            MessageBox.Show(message);
        }
    }
}
