﻿using CurrencyConverter.Client.Services;
using CurrencyConverter.Client.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CurrencyConverter.Client
{
    public class MainWindowViewModel : ViewModelBase
    {
        public readonly ConvertionServiceProxy Proxy;

        public MainWindowViewModel()
        {
            Proxy = new ConvertionServiceProxy();
            ButtonCommand = new AsyncCommand(ButtonExecute);
        }

        #region UI bindable properties

        public string Input { get; set; }

        public string Output { get; set; }

        public ICommand ButtonCommand { get; set; }

        #endregion




        public async Task ButtonExecute()
        {
            //Output = await Proxy.Convert(Input);
            //RaisePropertyChenged("Output");




        }

    }




    public class DerivedClass : BaseClass
    {
        public DerivedClass(int number, string text)
        {
            Number = number;
            Text = text;
        }
    }


    public class BaseClass
    {
        public int Number { get; protected set; }

        public string Text { get; protected set; }
    }


    public interface IYesIKnow
    {
        string Confirmation { get; }
    }





    public struct SowaStruct<T>
    {
        public T GenericMember { get; set; }


        public int Integer { get; set; }

        public string Text { get; set; }

        public DerivedClass ClassInstance { get; set; } // TODO

        public event EventHandler XEvent; // TODO

        public static int Number = 3;
    }
}
