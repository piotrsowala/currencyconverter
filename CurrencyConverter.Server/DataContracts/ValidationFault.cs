﻿using System.Runtime.Serialization;

namespace CurrencyConverter.Server.DataContracts
{
    [DataContract]
    public class ValidationFault
    {
        [DataMember]
        public string Message { get; set; }
    }
}
