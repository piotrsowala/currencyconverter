﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CurrencyConverter.Server.Utils
{
    public class DecimalParser
    {
        private const string _errorMessage = "Cannot parse number due to invalid number format.\nProper format is: #########,##";

        public decimal Parse(string input)
        {
            if (HasValidFormat(input) == false)
            {
                throw new ArgumentException(_errorMessage);
            }

            if (Decimal.TryParse(input, NumberStyles.Number, GetCustomCulture(), out decimal result) == false)
            {
                throw new ArgumentException(_errorMessage);
            }

            return result;
        }

        private bool HasValidFormat(string input)
        {
            return input != null && new Regex(@"^\d{1,9}(,\d{1,2})?$").IsMatch(input);
        }

        private CultureInfo GetCustomCulture()
        {
            var culture = new CultureInfo(CultureInfo.CurrentCulture.Name);
            culture.NumberFormat.NumberDecimalSeparator = ",";
            culture.NumberFormat.NumberDecimalDigits = 2;
            culture.NumberFormat.NumberGroupSeparator = " ";
            return culture;
        }
    }
}
