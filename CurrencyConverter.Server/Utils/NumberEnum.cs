﻿using System.ComponentModel.DataAnnotations;

namespace CurrencyConverter.Server.Utils
{
    public enum NumberEnum
    {
        [Display(Name = "zero")]
        N0 = 0,

        [Display(Name = "one")]
        N1 = 1,

        [Display(Name = "two")]
        N2 = 2,

        [Display(Name = "three")]
        N3 = 3,

        [Display(Name = "four")]
        N4 = 4,

        [Display(Name = "five")]
        N5 = 5,

        [Display(Name = "six")]
        N6 = 6,

        [Display(Name = "seven")]
        N7 = 7,

        [Display(Name = "eight")]
        N8 = 8,

        [Display(Name = "nine")]
        N9 = 9,

        [Display(Name = "ten")]
        N10 = 10,



        [Display(Name = "eleven")]
        N11 = 11,

        [Display(Name = "twelve")]
        N12 = 12,

        [Display(Name = "thirteen")]
        N13 = 13,

        [Display(Name = "fourteen")]
        N14 = 14,

        [Display(Name = "fifteen")]
        N15 = 15,

        [Display(Name = "sixteen")]
        N16 = 16,

        [Display(Name = "seventeen")]
        N17 = 17,

        [Display(Name = "eighteen")]
        N18 = 18,

        [Display(Name = "nineteen")]
        N19 = 19,

        [Display(Name = "twenty")]
        N20 = 20,



        [Display(Name = "thirty")]
        N30 = 30,

        [Display(Name = "forty")]
        N40 = 40,

        [Display(Name = "fifty")]
        N50 = 50,

        [Display(Name = "sixty")]
        N60 = 60,

        [Display(Name = "seventy")]
        N70 = 70,

        [Display(Name = "eighty")]
        N80 = 80,

        [Display(Name = "ninety")]
        N90 = 90,

        [Display(Name = "hundred")]
        N100 = 100,



        [Display(Name = "thousand")]
        N1000 = 1000,

        [Display(Name = "million")]
        N1000000 = 1000000,

        [Display(Name = "billion")]
        N1000000000 = 1000000000,
    }

}
