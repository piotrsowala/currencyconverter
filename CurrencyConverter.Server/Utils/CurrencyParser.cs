﻿using System;
using System.Linq;

namespace CurrencyConverter.Server.Utils
{
    public class CurrencyParser
    {
        private const string Dash = "-";
        private const string Space = " ";
        private const string And = " and ";


        public string Parse(decimal amount)
        {
            if(amount < 0)
            {
                throw new ArgumentException($"{nameof(Parse)} - argument cannot be negative");
            }

            var integralPart = (int)decimal.Floor(amount);
            var fractionPart = (int)((amount - integralPart) * 100);

            var dollarUnit = 1 == integralPart ? "dollar" : "dollars";
            var centUnit = 1 == fractionPart ? "cent" : "cents";

            var dollarPartString = integralPart > 0 ? $"{ParseNumber(integralPart)} {dollarUnit}" : $"{NumberEnum.N0.GetDisplay()} {dollarUnit}";
            var centPartString = fractionPart > 0 ? $"{ParseNumber(fractionPart)} {centUnit}" : String.Empty;

            return JoinStrings(dollarPartString, centPartString, And);
        }

        private string ParseNumber(int number, int thousantPower = 0)
        {
            var underThousand = number % 1000;
            var overThousand = number / 1000;

            var multiplier = (NumberEnum)Math.Pow(1000, thousantPower);
            var multiplierString = multiplier > NumberEnum.N1 ? multiplier.GetDisplay() : String.Empty;
            var underThousandString = JoinStrings(ParseUnderThousand(underThousand), multiplierString);
            var overThousandString = overThousand > 0 ? ParseNumber(overThousand, ++thousantPower) : String.Empty;

            return JoinStrings(overThousandString, underThousandString);
        }

        private string ParseUnderThousand(int number)
        {
            if (number >= 1000)
            {
                throw new ArgumentException($"{nameof(ParseUnderThousand)} - argument should be lower than 1000");
            }

            var underHundred = number % 100;
            var hundreds = (NumberEnum)(number / 100);

            var hundredsString = hundreds > 0 ? $"{hundreds.GetDisplay()} hundred" : String.Empty;
            var underHundredString = underHundred > 0 ? ParseUnderHundred(underHundred) : String.Empty;

            return JoinStrings(hundredsString, underHundredString);
        }

        private string ParseUnderHundred(int number)
        {
            if (number >= 100)
            {
                throw new ArgumentException($"{nameof(ParseUnderHundred)} - argument should be lower than 100");
            }

            string result;
            if (number < 20)
            {             
                result = ((NumberEnum)number).GetDisplay();
            }
            else
            {
                var units = (NumberEnum)(number % 10);
                var tens = (NumberEnum)(number - units);

                var unityString = units > 0 ? units.GetDisplay() : String.Empty;
                var tenString = tens > 0 ? tens.GetDisplay() : String.Empty;

                result = JoinStrings(tenString, unityString, Dash);
            }

            return result;
        }

        private string JoinStrings(string first, string second, string connector = Space)
        {
            var nonEmptyCollection = new [] { first, second }.Where(x => string.IsNullOrEmpty(x) == false);
            return string.Join(connector, nonEmptyCollection);
        }
    }
}
