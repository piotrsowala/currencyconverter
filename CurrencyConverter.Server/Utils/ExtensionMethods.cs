﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace CurrencyConverter.Server.Utils
{
    public static class ExtensionMethods
    {
        public static string GetDisplay(this NumberEnum input)
        {
            var attr = input.GetAttribute<DisplayAttribute>();
            return attr?.Name;
        }

        public static T GetAttribute<T>(this NumberEnum input) where T: Attribute
        {
            var memberInfo = input.GetType().GetMember(input.ToString()).First();
            return memberInfo.GetCustomAttribute<T>();
        }
    }
}
