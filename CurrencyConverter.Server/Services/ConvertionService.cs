﻿using System;
using System.ServiceModel;
using CurrencyConverter.Server.Utils;
using CurrencyConverter.Server.DataContracts;
using CurrencyConverter.Server.ServiceContracts;

namespace CurrencyConverter.Server.Services
{
    public class ConvertionService : IConvertionService
    {
        public string Convert(string input)
        {
            var decimalParser = new DecimalParser();

            decimal decimalValue;
            try
            {
                decimalValue = decimalParser.Parse(input);
            }
            catch (ArgumentException ex)
            {
                throw new FaultException<ValidationFault>(new ValidationFault { Message = ex.Message });
            }

            var currencyParser = new CurrencyParser();
            return currencyParser.Parse(decimalValue);
        }
    }
}
