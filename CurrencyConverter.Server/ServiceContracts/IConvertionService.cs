﻿using System.ServiceModel;
using CurrencyConverter.Server.DataContracts;

namespace CurrencyConverter.Server.ServiceContracts
{
    [ServiceContract]
    public interface IConvertionService
    {
        [OperationContract]
        [FaultContract(typeof(ValidationFault))]
        string Convert(string input);
    }

}
